
# LIBRARY HAS BEEN MOVED TO GITLAB. FOLLOW [THIS LINK](https://gitlab.com/enricos83/PPMAJAL-Expressive-PDDL-Java-Library)

# What is PPMaJal?

This repository contains the PPMaJal API, which is a planning manager library meant to build systems that speak the PDDL language. It supports many features that go way beyond classical planning, such as numeric representations, linear and non-linear constraints, autonomout processes, global constraints and other things for plan repair. Also there are API for generating SMT formulae and for handling initial state in form of beliefs.

This API has been used as basis for many projects, all related to Automated Planning, Replanning, Plan Execution, SMT Planning and others that I cannot remember now.

For more information on its applications have a look at some of the papers in [Google Scholar](https://scholar.google.com.au/citations?user=lgfpklAAAAAJ&hl=en)

The most recent advances of the library refer to:

E. Scala, P. Haslum, S. Thiebaux: **Heuristics for Numeric Planning via Subgoaling**, IJCAI 2016

E. Scala, M. Ramirez, P. Haslum, S. Thiebaux: **Numeric Planning with Disjunctive Global Constraints via SMT**, ICAPS 2016

E. Scala, P. Torasso: **Deordering and Numeric Macro Actions for Plan Repair**, IJCAI 2015

E. Scala, P. Haslum, S. Thiebaux, M. Ramirez: **Interval-Based Relaxation for General Numeric Planning**, ECAI 2016

E. Scala, P. Haslum, D. Magazzeni, S. Thiebaux: **Landmarks for Numeric Planning Problems**, IJCAI 2017

A. Grastien, E. Scala **Intelligent Belief State Sampling for Conformant Planning**, IJCAI 2017


Planners built on top of PPMaJaL can be downloaded following these other bitbucket repositories:

*The SMT Planner* can be downloaded from [here](https://bitbucket.org/enricode/springroll-smt-hybrid-planner)

*The ENHSP Planner* can be downloaded from [here](https://bitbucket.org/enricode/the-enhsp-planner). This is a heuristic forward search planner for PDDL+ problems

*The CPCES Planner* can be downloaded from [here](https://bitbucket.org/enricode/the-enhsp-planner). This is a conformant planner based on the idea of intelligently sampling the initial state, try and in case repair it. 


## Dependencies

The library depends on a number of libs, some for the PDDL parsing, other for some standard algorithm on graphs, and some to interface the API with Linear Program solvers:

In particular:

- [Antlr 3.4](http://www.antlr3.org) is used for parsing pddl problems. [Here](http://www.antlr3.org/download/antlr-3.4-complete.jar) the link to the actual library file that needs to be linked
- [Jgraph](http://jgrapht.org). This is for general algorithms on graphs
- [Ojalgo](http://ojalgo.org). The version used is the v40
- [Json Simple](https://github.com/fangyidong/json-simple). This is used to store information of the search space explored.

They are all open source projects, so is this library. For your convenience, the necessary jar files are all under the lib folder.

The library also depends on CPLEX 12.7.3, which is only required for solving cost-partitioning using landmarks. If you don't want to use it just disable each file containing it. Note that CPLEX is not free so we can't distribute it.

## Compilation

After having made sure to have JAVA 1.8 installed in your computer, and ant, just type 'ant dist' in the root directory. The jar to distribute will be located under dist/lib

